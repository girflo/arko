# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.2.4] - 2024-09-18

### Added

- Unit tests

### Changed

- Update palette to v0.7.6
- Update image to v0.25.2

### Fixed

- Fix Brush top to bottom and left to right

## [0.2.3] - 2022-08-25

### Changed

- Update palette to v0.6.0

## [0.2.2] - 2022-08-25

### Changed

- Update dependencies

## [0.2.1] - 2022-04-17

### Added

- Add clone trait to Colors

## [0.2.0] - 2022-04-17

### Changed

- slim now use Colors from an newly created enum
