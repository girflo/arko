use crate::validator;

pub struct IO<'a> {
    pub in_img: image::DynamicImage,
    pub out_img: &'a str
}

pub fn build_io<'a>(in_path: &'a str, out_path: &'a str, force_ouput_overwrite: bool) -> IO<'a> {
    validator::files_presence(in_path, out_path, force_ouput_overwrite);
    let img = image::open(in_path).unwrap();
    IO {
        in_img: img,
        out_img: out_path,
    }
}
