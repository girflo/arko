mod brush;
pub mod colors;
pub use colors::Colors;
mod io;
mod pixel_color;
mod pixel_sort;
mod pixel_sort_brut;
mod pixel_sorting;
mod slim;
mod validator;

pub fn brush_bottom_to_top(
    in_img: &str,
    out_img: &str,
    force_ouput_overwrite: bool,
    proba: i32,
    min: i32,
    max: i32,
) {
    let io = io::build_io(in_img, out_img, force_ouput_overwrite);
    brush::bottom_to_top(io, proba, min, max)
}

pub fn brush_left_to_right(
    in_img: &str,
    out_img: &str,
    force_ouput_overwrite: bool,
    proba: i32,
    min: i32,
    max: i32,
) {
    let io = io::build_io(in_img, out_img, force_ouput_overwrite);
    brush::left_to_right(io, proba, min, max)
}

pub fn brush_right_to_left(
    in_img: &str,
    out_img: &str,
    force_ouput_overwrite: bool,
    proba: i32,
    min: i32,
    max: i32,
) {
    let io = io::build_io(in_img, out_img, force_ouput_overwrite);
    brush::right_to_left(io, proba, min, max)
}

pub fn brush_top_to_bottom(
    in_img: &str,
    out_img: &str,
    force_ouput_overwrite: bool,
    proba: i32,
    min: i32,
    max: i32,
) {
    let io = io::build_io(in_img, out_img, force_ouput_overwrite);
    brush::top_to_bottom(io, proba, min, max)
}

pub fn convert(in_img: &str, out_img: &str, force_ouput_overwrite: bool) {
    validator::files_presence(in_img, out_img, force_ouput_overwrite);
    let img = image::open(in_img).unwrap();

    img.save(out_img).unwrap();
}

pub fn slim_bottom_to_top_global(
    in_img: &str,
    out_img: &str,
    force_ouput_overwrite: bool,
    proba: i32,
    colors: Vec<colors::Colors>,
) {
    let io = io::build_io(in_img, out_img, force_ouput_overwrite);
    slim::bottom_to_top_global(io, proba, colors)
}

pub fn slim_left_to_right_global(
    in_img: &str,
    out_img: &str,
    force_ouput_overwrite: bool,
    proba: i32,
    colors: Vec<colors::Colors>,
) {
    let io = io::build_io(in_img, out_img, force_ouput_overwrite);
    slim::left_to_right_global(io, proba, colors)
}

pub fn slim_right_to_left_global(
    in_img: &str,
    out_img: &str,
    force_ouput_overwrite: bool,
    proba: i32,
    colors: Vec<colors::Colors>,
) {
    let io = io::build_io(in_img, out_img, force_ouput_overwrite);
    slim::right_to_left_global(io, proba, colors)
}

pub fn slim_top_to_bottom_global(
    in_img: &str,
    out_img: &str,
    force_ouput_overwrite: bool,
    proba: i32,
    colors: Vec<colors::Colors>,
) {
    let io = io::build_io(in_img, out_img, force_ouput_overwrite);
    slim::top_to_bottom_global(io, proba, colors)
}

pub fn slim_bottom_to_top_per_color(
    in_img: &str,
    out_img: &str,
    force_ouput_overwrite: bool,
    colors_proba: Vec<(colors::Colors, i32)>,
) {
    let io = io::build_io(in_img, out_img, force_ouput_overwrite);
    slim::bottom_to_top_per_color(io, colors_proba)
}

pub fn slim_left_to_right_per_color(
    in_img: &str,
    out_img: &str,
    force_ouput_overwrite: bool,
    colors_proba: Vec<(colors::Colors, i32)>,
) {
    let io = io::build_io(in_img, out_img, force_ouput_overwrite);
    slim::left_to_right_per_color(io, colors_proba)
}

pub fn slim_right_to_left_per_color(
    in_img: &str,
    out_img: &str,
    force_ouput_overwrite: bool,
    colors_proba: Vec<(colors::Colors, i32)>,
) {
    let io = io::build_io(in_img, out_img, force_ouput_overwrite);
    slim::right_to_left_per_color(io, colors_proba)
}

pub fn slim_top_to_bottom_per_color(
    in_img: &str,
    out_img: &str,
    force_ouput_overwrite: bool,
    colors_proba: Vec<(colors::Colors, i32)>,
) {
    let io = io::build_io(in_img, out_img, force_ouput_overwrite);
    slim::top_to_bottom_per_color(io, colors_proba)
}

pub fn sort_brut_bottom_to_top(in_img: &str, out_img: &str, force_ouput_overwrite: bool) {
    let io = io::build_io(in_img, out_img, force_ouput_overwrite);
    pixel_sort_brut::bottom_to_top(io)
}

pub fn sort_brut_left_to_right(in_img: &str, out_img: &str, force_ouput_overwrite: bool) {
    let io = io::build_io(in_img, out_img, force_ouput_overwrite);
    pixel_sort_brut::left_to_right(io)
}

pub fn sort_brut_right_to_left(in_img: &str, out_img: &str, force_ouput_overwrite: bool) {
    let io = io::build_io(in_img, out_img, force_ouput_overwrite);
    pixel_sort_brut::right_to_left(io)
}

pub fn sort_brut_top_to_bottom(in_img: &str, out_img: &str, force_ouput_overwrite: bool) {
    let io = io::build_io(in_img, out_img, force_ouput_overwrite);
    pixel_sort_brut::top_to_bottom(io)
}

pub fn sort_bottom_to_top(
    in_img: &str,
    out_img: &str,
    force_ouput_overwrite: bool,
    detection_type: i32,
    detection_min: i32,
    detection_max: i32,
    multiple_range: bool,
    detection_min_2: i32,
    detection_max_2: i32,
    sorting_by: i32,
) {
    let io = io::build_io(in_img, out_img, force_ouput_overwrite);
    pixel_sort::bottom_to_top(
        io,
        detection_type,
        detection_min,
        detection_max,
        multiple_range,
        detection_min_2,
        detection_max_2,
        sorting_by,
    )
}

pub fn sort_left_to_right(
    in_img: &str,
    out_img: &str,
    force_ouput_overwrite: bool,
    detection_type: i32,
    detection_min: i32,
    detection_max: i32,
    multiple_range: bool,
    detection_min_2: i32,
    detection_max_2: i32,
    sorting_by: i32,
) {
    let io = io::build_io(in_img, out_img, force_ouput_overwrite);
    pixel_sort::left_to_right(
        io,
        detection_type,
        detection_min,
        detection_max,
        multiple_range,
        detection_min_2,
        detection_max_2,
        sorting_by,
    )
}

pub fn sort_right_to_left(
    in_img: &str,
    out_img: &str,
    force_ouput_overwrite: bool,
    detection_type: i32,
    detection_min: i32,
    detection_max: i32,
    multiple_range: bool,
    detection_min_2: i32,
    detection_max_2: i32,
    sorting_by: i32,
) {
    let io = io::build_io(in_img, out_img, force_ouput_overwrite);
    pixel_sort::right_to_left(
        io,
        detection_type,
        detection_min,
        detection_max,
        multiple_range,
        detection_min_2,
        detection_max_2,
        sorting_by,
    )
}

pub fn sort_top_to_bottom(
    in_img: &str,
    out_img: &str,
    force_ouput_overwrite: bool,
    detection_type: i32,
    detection_min: i32,
    detection_max: i32,
    multiple_range: bool,
    detection_min_2: i32,
    detection_max_2: i32,
    sorting_by: i32,
) {
    let io = io::build_io(in_img, out_img, force_ouput_overwrite);
    pixel_sort::top_to_bottom(
        io,
        detection_type,
        detection_min,
        detection_max,
        multiple_range,
        detection_min_2,
        detection_max_2,
        sorting_by,
    )
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;
    use std::path::Path;

    #[test]
    fn brush_bottom_to_top_works() {
        brush_bottom_to_top(
            "test/test.png",
            "test/brush_bottom_to_top.png",
            true,
            100,
            4,
            20,
        );
        assert!(Path::new("test/brush_bottom_to_top.png").exists());
        fs::remove_file("test/brush_bottom_to_top.png").unwrap();
    }

    #[test]
    fn brush_top_to_bottom_works() {
        brush_top_to_bottom(
            "test/test.png",
            "test/brush_top_to_bottom.png",
            true,
            100,
            4,
            20,
        );
        assert!(Path::new("test/brush_top_to_bottom.png").exists());
        fs::remove_file("test/brush_top_to_bottom.png").unwrap();
    }

    #[test]
    fn brush_left_to_right_works() {
        brush_left_to_right(
            "test/test.png",
            "test/brush_left_to_right.png",
            true,
            100,
            4,
            20,
        );
        assert!(Path::new("test/brush_left_to_right.png").exists());
        fs::remove_file("test/brush_left_to_right.png").unwrap();
    }

    #[test]
    fn brush_right_to_left_works() {
        brush_right_to_left(
            "test/test.png",
            "test/brush_right_to_left.png",
            true,
            100,
            4,
            20,
        );
        assert!(Path::new("test/brush_right_to_left.png").exists());
        fs::remove_file("test/brush_right_to_left.png").unwrap();
    }

    #[test]
    fn slim_bottom_to_top_global_works() {
        let colors = vec![Colors::Red, Colors::White, Colors::Black];
        slim_bottom_to_top_global(
            "test/test.png",
            "test/slim_bottom_to_top_global.png",
            true,
            80,
            colors,
        );
        assert!(Path::new("test/slim_bottom_to_top_global.png").exists());
        fs::remove_file("test/slim_bottom_to_top_global.png").unwrap();
    }

    #[test]
    fn slim_top_to_bottom_global_works() {
        let colors = vec![Colors::Red, Colors::White, Colors::Black];
        slim_top_to_bottom_global(
            "test/test.png",
            "test/slim_top_to_bottom_global.png",
            true,
            80,
            colors,
        );
        assert!(Path::new("test/slim_top_to_bottom_global.png").exists());
        fs::remove_file("test/slim_top_to_bottom_global.png").unwrap();
    }

    #[test]
    fn slim_left_to_right_global_works() {
        let colors = vec![Colors::Red, Colors::White, Colors::Black];
        slim_left_to_right_global(
            "test/test.png",
            "test/slim_left_to_right_global.png",
            true,
            70,
            colors,
        );
        assert!(Path::new("test/slim_left_to_right_global.png").exists());
        fs::remove_file("test/slim_left_to_right_global.png").unwrap();
    }

    #[test]
    fn slim_right_to_left_global_works() {
        let colors = vec![Colors::Red, Colors::White, Colors::Black];
        slim_right_to_left_global(
            "test/test.png",
            "test/slim_right_to_left_global.png",
            true,
            70,
            colors,
        );
        assert!(Path::new("test/slim_right_to_left_global.png").exists());
        fs::remove_file("test/slim_right_to_left_global.png").unwrap();
    }

    #[test]
    fn slim_bottom_to_top_per_color_works() {
        slim_bottom_to_top_per_color(
            "test/test.png",
            "test/slim_bottom_to_top_per_color.png",
            true,
            vec![
                (Colors::Red, 20),
                (Colors::Yellow, 34),
                (Colors::Green, 56),
                (Colors::Cyan, 45),
                (Colors::Blue, 64),
                (Colors::Magenta, 80),
                (Colors::White, 80),
                (Colors::Black, 70),
            ],
        );
        assert!(Path::new("test/slim_bottom_to_top_per_color.png").exists());
        fs::remove_file("test/slim_bottom_to_top_per_color.png").unwrap();
    }

    #[test]
    fn slim_top_to_bottom_per_color_works() {
        slim_top_to_bottom_per_color(
            "test/test.png",
            "test/slim_top_to_bottom_per_color.png",
            true,
            vec![
                (Colors::Red, 20),
                (Colors::Yellow, 34),
                (Colors::Green, 56),
                (Colors::Cyan, 45),
                (Colors::Blue, 64),
                (Colors::Magenta, 23),
                (Colors::White, 80),
                (Colors::Black, 70),
            ],
        );
        assert!(Path::new("test/slim_top_to_bottom_per_color.png").exists());
        fs::remove_file("test/slim_top_to_bottom_per_color.png").unwrap();
    }

    #[test]
    fn slim_left_to_right_per_color_works() {
        slim_left_to_right_per_color(
            "test/test.png",
            "test/slim_left_to_right_per_color.png",
            true,
            vec![
                (Colors::Red, 20),
                (Colors::Yellow, 34),
                (Colors::Green, 56),
                (Colors::Cyan, 45),
                (Colors::Blue, 64),
                (Colors::Magenta, 23),
                (Colors::White, 80),
                (Colors::Black, 70),
            ],
        );
        assert!(Path::new("test/slim_left_to_right_per_color.png").exists());
        fs::remove_file("test/slim_left_to_right_per_color.png").unwrap();
    }

    #[test]
    fn slim_right_to_left_per_color_works() {
        slim_right_to_left_per_color(
            "test/test.png",
            "test/slim_right_to_left_per_color.png",
            true,
            vec![
                (Colors::Red, 20),
                (Colors::Yellow, 34),
                (Colors::Green, 56),
                (Colors::Cyan, 45),
                (Colors::Blue, 64),
                (Colors::Magenta, 23),
                (Colors::White, 80),
                (Colors::Black, 70),
            ],
        );
        assert!(Path::new("test/slim_right_to_left_per_color.png").exists());
        fs::remove_file("test/slim_right_to_left_per_color.png").unwrap();
    }

    #[test]
    fn sort_brut_bottom_to_top_works() {
        sort_brut_bottom_to_top("test/test.png", "test/sort_brut_bottom_to_top.png", true);
        assert!(Path::new("test/sort_brut_bottom_to_top.png").exists());
        fs::remove_file("test/sort_brut_bottom_to_top.png").unwrap();
    }

    #[test]
    fn sort_brut_top_to_bottom_works() {
        sort_brut_top_to_bottom("test/test.png", "test/sort_brut_top_to_bottom.png", true);
        assert!(Path::new("test/sort_brut_top_to_bottom.png").exists());
        fs::remove_file("test/sort_brut_top_to_bottom.png").unwrap();
    }

    #[test]
    fn sort_brut_left_to_right_works() {
        sort_brut_left_to_right("test/test.png", "test/sort_brut_left_to_right.png", true);
        assert!(Path::new("test/sort_brut_left_to_right.png").exists());
        fs::remove_file("test/sort_brut_left_to_right.png").unwrap();
    }

    #[test]
    fn sort_brut_right_to_left_works() {
        sort_brut_right_to_left("test/test.png", "test/sort_brut_right_to_left.png", true);
        assert!(Path::new("test/sort_brut_right_to_left.png").exists());
        fs::remove_file("test/sort_brut_right_to_left.png").unwrap();
    }

    #[test]
    fn sort_bottom_to_top_works() {
        sort_bottom_to_top(
            "test/test.png",
            "test/sort_bottom_to_top.png",
            true,
            0,
            1,
            360,
            false,
            0,
            0,
            1,
        );
        assert!(Path::new("test/sort_bottom_to_top.png").exists());
        fs::remove_file("test/sort_bottom_to_top.png").unwrap();
    }

    #[test]
    fn sort_top_to_bottom_works() {
        sort_top_to_bottom(
            "test/test.png",
            "test/sort_top_to_bottom.png",
            true,
            0,
            1,
            360,
            false,
            0,
            0,
            1,
        );
        assert!(Path::new("test/sort_top_to_bottom.png").exists());
        fs::remove_file("test/sort_top_to_bottom.png").unwrap();
    }

    #[test]
    fn sort_left_to_right_works() {
        sort_left_to_right(
            "test/test.png",
            "test/sort_left_to_right.png",
            true,
            0,
            1,
            360,
            false,
            0,
            0,
            0,
        );
        assert!(Path::new("test/sort_left_to_right.png").exists());
        fs::remove_file("test/sort_left_to_right.png").unwrap();
    }

    #[test]
    fn sort_right_to_left_works() {
        sort_right_to_left(
            "test/test.png",
            "test/sort_right_to_left.png",
            true,
            0,
            1,
            360,
            false,
            0,
            0,
            1,
        );
        assert!(Path::new("test/sort_right_to_left.png").exists());
        fs::remove_file("test/sort_right_to_left.png").unwrap();
    }

    #[test]
    fn convert_works() {
        convert("test/test.png", "test/test.jpg", true);
        assert!(Path::new("test/test.jpg").exists());
        fs::remove_file("test/test.jpg").unwrap();
    }
}
