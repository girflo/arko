use crate::io::IO;
use crate::pixel_sorting::PixelSorting;
use image::GenericImageView;

pub fn bottom_to_top(io: IO) {
    let pixel_sort_brut = PixelSortBrut { io };
    pixel_sort_brut.bottom_to_top()
}

pub fn left_to_right(io: IO) {
    let pixel_sort_brut = PixelSortBrut { io };
    pixel_sort_brut.left_to_right()
}

pub fn right_to_left(io: IO) {
    let pixel_sort_brut = PixelSortBrut { io };
    pixel_sort_brut.right_to_left()
}

pub fn top_to_bottom(io: IO) {
    let pixel_sort_brut = PixelSortBrut { io };
    pixel_sort_brut.top_to_bottom()
}

struct PixelSortBrut<'a> {
    io: IO<'a>,
}

impl<'a> PixelSortBrut<'a> {
    fn bottom_to_top(&self) {
        let mut new_img = image::ImageBuffer::new(self.io.in_img.width(), self.io.in_img.height());
        for w in 0..(self.io.in_img.width() - 1) {
            let mut strip = Vec::new();
            strip.push(self.io.in_img.get_pixel(w, self.io.in_img.height() - 1));
            for h in 1..(self.io.in_img.height() - 1) {
                strip.push(self.io.in_img.get_pixel(w, self.io.in_img.height() - 1 - h));
            }
            let new_strip = self.sort_strip_by_hue(&mut strip);
            self.prepend_strip_to_image_vertically(
                &mut new_img,
                new_strip,
                self.io.in_img.height() - 1,
                w,
            );
        }
        new_img.save(self.io.out_img).unwrap();
    }

    fn left_to_right(&self) {
        let mut new_img = image::ImageBuffer::new(self.io.in_img.width(), self.io.in_img.height());
        for h in 0..(self.io.in_img.height() - 1) {
            let mut strip = Vec::new();
            strip.push(self.io.in_img.get_pixel(0, h));
            for w in 1..(self.io.in_img.width() - 1) {
                strip.push(self.io.in_img.get_pixel(w, h));
            }
            let new_strip = self.sort_strip_by_hue(&mut strip);
            self.append_strip_to_image(&mut new_img, new_strip, h, 0);
        }
        new_img.save(self.io.out_img).unwrap();
    }

    fn right_to_left(&self) {
        let mut new_img = image::ImageBuffer::new(self.io.in_img.width(), self.io.in_img.height());
        for h in 0..(self.io.in_img.height() - 1) {
            let mut strip = Vec::new();
            strip.push(self.io.in_img.get_pixel(self.io.in_img.width() - 1, h));
            for w in 1..(self.io.in_img.width() - 1) {
                strip.push(self.io.in_img.get_pixel(self.io.in_img.width() - 1 - w, h));
            }
            let new_strip = self.sort_strip_by_hue(&mut strip);
            self.prepend_strip_to_image(&mut new_img, new_strip, h, self.io.in_img.width() - 1);
        }
        new_img.save(self.io.out_img).unwrap();
    }

    fn top_to_bottom(&self) {
        let mut new_img = image::ImageBuffer::new(self.io.in_img.width(), self.io.in_img.height());
        for w in 0..(self.io.in_img.width() - 1) {
            let mut strip = Vec::new();
            strip.push(self.io.in_img.get_pixel(w, 0));
            for h in 1..(self.io.in_img.height() - 1) {
                strip.push(self.io.in_img.get_pixel(w, h));
            }
            let new_strip = self.sort_strip_by_hue(&mut strip);
            self.append_strip_to_image_vertically(&mut new_img, new_strip, 0, w);
        }
        new_img.save(self.io.out_img).unwrap();
    }
}
impl<'a> PixelSorting for PixelSortBrut<'a> {}
