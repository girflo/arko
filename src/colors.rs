#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Colors {
    Black,
    Blue,
    Cyan,
    Green,
    Grey,
    Magenta,
    Red,
    White,
    Yellow,
}
