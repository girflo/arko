use crate::colors::Colors;
pub mod pixel_color {
    use crate::colors::Colors;
    use image::Pixel;
    use palette::{Hsl, IntoColor};
    use std::f32::consts::PI;

    pub fn get_pixel_color<'a>(pixel: image::Rgba<u8>) -> Colors {
        let lig = get_lig(pixel);
        let sat = get_sat(pixel);
        let hue = get_hue(pixel);

        if lig > 392923.0 {
            Colors::White
        } else if lig < 2407.0 {
            Colors::Black
        } else if sat > -0.25 {
            Colors::Grey
        } else if hue < 0.09 {
            Colors::Red
        } else if hue >= 0.09 && hue < 0.19 {
            Colors::Yellow
        } else if hue >= 0.19 && hue < 0.36 {
            Colors::Green
        } else if hue >= 0.36 && hue < 0.56 {
            Colors::Cyan
        } else if hue >= 0.56 && hue < 0.68 {
            Colors::Blue
        } else if hue >= 0.68 && hue < 0.98 {
            Colors::Magenta
        } else {
            Colors::Red
        }
    }

    pub fn get_hue(pixel: image::Rgba<u8>) -> f32 {
        pixel_to_hsl(pixel).hue.into_positive_radians() / (2.0 * PI)
    }

    pub fn get_lig(pixel: image::Rgba<u8>) -> f32 {
        pixel_to_hsl(pixel).lightness
    }

    pub fn get_sat(pixel: image::Rgba<u8>) -> f32 {
        pixel_to_hsl(pixel).saturation
    }

    fn pixel_to_hsl(pixel: image::Rgba<u8>) -> palette::Hsl {
        let rgb = pixel_to_rgb(pixel);
        let hsl: Hsl = rgb.into_color();
        hsl
    }

    fn pixel_to_rgb(pixel: image::Rgba<u8>) -> palette::rgb::Rgb {
        let values = pixel.to_rgb();
        let red = f32::from(*values.0.iter().nth(0).unwrap());
        let green = f32::from(*values.0.iter().nth(1).unwrap());
        let blue = f32::from(*values.0.iter().nth(2).unwrap());
        palette::rgb::Rgb::new(red, green, blue)
    }
}

pub fn get_pixel_color<'a>(pixel: image::Rgba<u8>) -> Colors {
    pixel_color::get_pixel_color(pixel)
}

pub fn get_pixel_hue<'a>(pixel: image::Rgba<u8>) -> f32 {
    pixel_color::get_hue(pixel)
}

pub fn get_pixel_lightness<'a>(pixel: image::Rgba<u8>) -> f32 {
    pixel_color::get_lig(pixel)
}

pub fn get_pixel_saturation<'a>(pixel: image::Rgba<u8>) -> f32 {
    pixel_color::get_sat(pixel)
}
