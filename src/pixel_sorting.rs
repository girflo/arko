use crate::pixel_color;
use image::Rgba;

pub trait PixelSorting {
    fn append_strip_to_image<'c>(&self, image: &'c mut image::ImageBuffer<Rgba<u8>, Vec<u8>>, strip: &'c mut Vec<image::Rgba<u8>>, h: u32, w: u32) -> &'c mut image::ImageBuffer<Rgba<u8>, Vec<u8>> {
        for p in 0..strip.len() {
            let formatted_p = p as u32;
            image.put_pixel(w + formatted_p, h, strip[p]);
        }
        image
    }

    fn append_strip_to_image_vertically<'c>(&self, image: &'c mut image::ImageBuffer<Rgba<u8>, Vec<u8>>, strip: &'c mut Vec<image::Rgba<u8>>, h: u32, w: u32) -> &'c mut image::ImageBuffer<Rgba<u8>, Vec<u8>> {
        for p in 0..strip.len() {
            let formatted_p = p as u32;
            image.put_pixel(w, h + formatted_p, strip[p]);
        }
        image
    }

    fn prepend_strip_to_image_vertically<'c>(&self, image: &'c mut image::ImageBuffer<Rgba<u8>, Vec<u8>>, strip: &'c mut Vec<image::Rgba<u8>>, h: u32, w: u32) -> &'c mut image::ImageBuffer<Rgba<u8>, Vec<u8>> {
        for p in 0..strip.len() {
            let formatted_p = p as u32;
            let position_in_strip = strip.len() - p - 1;
            image.put_pixel(w, h - formatted_p, strip[position_in_strip]);
        }
        image
    }

    fn prepend_strip_to_image<'c>(&self, image: &'c mut image::ImageBuffer<Rgba<u8>, Vec<u8>>, strip: &'c mut Vec<image::Rgba<u8>>, h: u32, w: u32) -> &'c mut image::ImageBuffer<Rgba<u8>, Vec<u8>> {
        for p in 0..strip.len() {
            let formatted_p = p as u32;
            let position_in_strip = strip.len() - p - 1;
            image.put_pixel(w - formatted_p, h, strip[position_in_strip]);
        }
        image
    }

    fn sort_strip_by_hue<'b>(&self, strip: &'b mut Vec<image::Rgba<u8>>) -> &'b mut Vec<image::Rgba<u8>> {
        strip.sort_by(|a, b| pixel_color::get_pixel_hue(*a).partial_cmp(&pixel_color::get_pixel_hue(*b)).unwrap());
        strip
    }
}
