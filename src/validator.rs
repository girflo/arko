pub fn files_presence(in_img: &str, out_img: &str, force_ouput_overwrite: bool) {
    let in_file = std::path::Path::new(in_img).exists();
    let out_file = std::path::Path::new(out_img).exists();
    if in_file == false {
        panic!("The input file provided doesn't exist")
    }
    if out_file == true && force_ouput_overwrite == false {
        panic!("The output file provided alredy exists")
    }
}

pub fn min_max(min: i32, max: i32) {
    if min > max {
        panic!("The value of min is above the value of max")
    }
}

pub fn percent(attribute: &str, value: i32) {
    if value < 0 || value > 100 {
        panic!("The value of {} should be between 0 and 100", attribute)
    }
}
